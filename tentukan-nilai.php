<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php
function tentukan_nilai($nilai)
{
    //  kode disini
 if($nilai <= 100 ) { $tentukan_nilai = "Sangat Baik <br>"; }
 if($nilai <  85 )  { $tentukan_nilai = "Baik <br>"; }
 if($nilai <  70 )  { $tentukan_nilai = "Cukup <br>"; }
 if($nilai <  60 )  { $tentukan_nilai = "Kurang <br>"; }

 return $tentukan_nilai;
}

//TEST CASES
echo "Nilai 98 = " . tentukan_nilai(98); //Sangat Baik
echo "Nilai 76 = " . tentukan_nilai(76); //Baik
echo "Nilai 67 = " . tentukan_nilai(67); //Cukup
echo "Nilai 43 = " . tentukan_nilai(43); //Kurang
?>
</body>
</html>
